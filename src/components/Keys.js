import React from 'react';
import './Keys.css'
import Key from './Key';
import {Howl, Howler} from 'howler';
import clap from '../sounds/clap.wav';
import hihat from '../sounds/hihat.wav';
import kick from '../sounds/kick.wav';
import openhat from '../sounds/openhat.wav';
import boom from '../sounds/boom.wav';
import ride from '../sounds/ride.wav';
import snare from '../sounds/snare.wav';
import tom from '../sounds/tom.wav';
import tink from '../sounds/tink.wav';

class Keys extends React.Component {
    componentDidMount() {
        document.addEventListener('keydown',this.playSound);
      }
      componentWillUnmount() {
        document.removeEventListener('keydown', this.playSound);
      }
    
      Soundplay = (src) =>{
        const sound = new Howl({
            src
        })
        sound.play()
      }
    
      playSound=(e)=>{
          e.preventDefault();
          console.log(e.keyCode);
          switch(e.keyCode){
              case 65:
                  this.Soundplay(clap);
                  break;
              case 83:
                  this.Soundplay(hihat);
                  break;
              case 68:
                  this.Soundplay(kick);
                  break;
              case 70:
                  this.Soundplay(openhat);
                  break;
              case 71:
                  this.Soundplay(boom);
                  break;
              case 72:
                  this.Soundplay(ride);
                  break;
              case 74:
                  this.Soundplay(snare);
                  break;
              case 75:
                  this.Soundplay(tom);
                  break;
              case 76:
                  this.Soundplay(tink);
                  break;
              default:
                  break;
              }
      }
      render(){
          Howler.volume(1.0);
        return(
            <form className="keys">
            <Key id="65" letter="A" sound="clap"/>
            <Key id="83" letter="S" sound="hihat"/>
            <Key id="68" letter="D" sound="kick"/>
            <Key id="70" letter="F" sound="openhat"/>
            <Key id="71" letter="G" sound="boom"/>
            <Key id="72" letter="H" sound="ride"/>
            <Key id="74" letter="J" sound="snare"/>
            <Key id="75" letter="K" sound="tom"/>
            <Key id="76" letter="L" sound="tink"/>
            </form>
        )
    }
}

export default Keys