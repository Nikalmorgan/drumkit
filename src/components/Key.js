import React from 'react';
import {Howl, Howler} from 'howler';
import './Key.css';
import clap from '../sounds/clap.wav';
import hihat from '../sounds/hihat.wav';
import kick from '../sounds/kick.wav';
import openhat from '../sounds/openhat.wav';
import boom from '../sounds/boom.wav';
import ride from '../sounds/ride.wav';
import snare from '../sounds/snare.wav';
import tom from '../sounds/tom.wav';
import tink from '../sounds/tink.wav';


class Key extends React.Component {
    soundPlay = (src) =>{
        const sound = new Howl({
            src
        })
        sound.play()
    }
    
    playSound=(e, sound, id)=>{
        e.preventDefault();
        let btnElement= document.getElementById(id);
        btnElement.className += "playing";
        console.log(sound);
        switch(sound){
            case "clap":
                this.soundPlay(clap);
                break;
            case "hihat":
                this.soundPlay(hihat);
                break;
            case "kick":
                this.soundPlay(kick);
                break;
            case "openhat":
                this.soundPlay(openhat);
                break;
            case "boom":
                this.soundPlay(boom);
                break;
            case "ride":
                this.soundPlay(ride);
                break;
            case "snare":
                this.soundPlay(snare);
                break;
            case "tom":
                this.soundPlay(tom);
                break;
            case "tink":
                this.soundPlay(tink);
                break;
            default:
                break;
            }
            btnElement.className = "key";
    }
    render(){
        Howler.volume(1.0);
        return(
            <button id={this.props.id} className="key" onClick={(e)=>{this.playSound(e, this.props.sound, this.props.id)}}>
                <kbd>{this.props.letter}</kbd>
                <span className="sound">{this.props.sound}</span>
            </button>
        )
    }
}

export default Key


